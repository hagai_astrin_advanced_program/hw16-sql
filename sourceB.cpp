#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <string>

#define AVAILABLE_INDEX 0
#define BALANCE_INDEX 4
#define PRICE_INDEX 5
#define FIRST_VALUE 0 // First value under specific culomn in a table.
#define NOT_AVAILABLE 0

using namespace std;

unordered_map<string, vector<string>> results;

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

bool checkIfCan(int buyerid, int carid, sqlite3* db)
{
	// Ex1 variables.
	/////////////////
	string available;
	string balance;
	string price;
	int index = 0;
	/////////////////

	// Ex2 variables.
	/////////////////
	string query;
	int money, carPrice, moneyLeft;
	int rc;
	char* zErrMsg;
	/////////////////

	// Get the requested values from the table.
	for (auto iter = results.begin(); iter != results.end(); iter++)
	{
		if (index == AVAILABLE_INDEX) // Get available value.
		{
			available = iter->second[FIRST_VALUE];
		}
		else if (index == BALANCE_INDEX) // Get balance value.
		{
			balance = iter->second[FIRST_VALUE];
		}
		else if (index == PRICE_INDEX) // GEt price value.
		{
			price = iter->second[FIRST_VALUE];
		}
		index++;
	}
	if (stoi(available) == NOT_AVAILABLE) // Car is not available.
	{
		return false;
	}
	else if ((stoi(balance) - stoi(price)) < 0) // Can't purchase the car.
	{
		return false;
	}
	else // Car can be purchased. 
	{
		money = stoi(balance);
		carPrice = stoi(price);
		moneyLeft = money - carPrice;

		clearTable();
		query = "update accounts set balance=" + std::to_string(moneyLeft) + " where id=" + std::to_string(buyerid);
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		clearTable();
		query = "update cars set available=0 where id=" + std::to_string(carid);
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		return true;
	}
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query;
	const char* sentQuery;

	string buyID = std::to_string(buyerid);
	string carID = std::to_string(carid);

	// Create the query.
	query = "select * from accounts join cars where accounts.buyer_id = " + buyID + " and cars.id = " + carID;
	sentQuery = query.c_str();

	rc = sqlite3_exec(db, sentQuery, callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		return checkIfCan(buyerid, carid, db); // Check if car can be purchased.
	}
}

bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query;
	int balanceFrom, balanceTo;

	// Get 'from' money amount
	query = "select * from accounts where buyer_id=" + std::to_string(from);
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	else
	{
		//printTable();
		auto iter = results.begin();
		iter++;
		iter++;
		balanceFrom = stoi(iter->second[FIRST_VALUE]);
		clearTable();
	}

	// Get 'to' money amount
	query = "select * from accounts where buyer_id=" + std::to_string(to);
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return false;
	}
	else
	{
		//printTable();
		auto iter = results.begin();
		iter++;
		iter++;
		balanceTo = stoi(iter->second[FIRST_VALUE]);
		clearTable();
	}

	if ((balanceFrom - amount) < 0) // Can't transfer money.
	{
		return false;
	}
	else
	{
		// Transaction command is needed here:
		query = "begin transaction";
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		query = "update accounts set balance=" + std::to_string(balanceFrom-amount) + " where buyer_id=" + std::to_string(from);
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		query = "update accounts set balance=" + std::to_string(balanceTo + amount) + " where buyer_id=" + std::to_string(to);
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		query = "commit";
		rc = sqlite3_exec(db, query.c_str(), NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return false;
		}

		return true; // If everything went fine, return true - money transfer completed!
	}
}

void whoCanBuy(int carId, sqlite3* db, char* zErrMsg)
{
	int rc;
	string query;

	// PART 1 OF BONUS:
	cout << "Part A of bonus:\n-----------------" << endl;
	query = "select accounts.buyer_id from accounts join cars where cars.id=" + std::to_string(carId) + " and accounts.balance > cars.price";
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else
	{
		printTable();
		clearTable();
	}
	
	cout << "\n\n";
	// PART 2 OF BONUS:
	cout << "Part b of bonus:\n-----------------" << endl;
	query = "select color, count(*) AS AmountOfCars from cars group by color";
	rc = sqlite3_exec(db, query.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else
	{
		printTable();
		clearTable();
	}
	
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}


	if (!carPurchase(1, 1, db, zErrMsg))
	{
		cout << "Unable to purchase, car is not available and its too expensive" << endl;
	}
	clearTable();
	if (carPurchase(8, 8, db, zErrMsg))
	{
		cout << "Purchase success!" << endl; 
	}
	clearTable();
	if (carPurchase(9, 9, db, zErrMsg))
	{
		cout << "Purchase success!" << endl;
	}
	clearTable();

	cout << "\n\n\n";

	if (balanceTransfer(1, 2, 54000, db, zErrMsg))
	{
		cout << "\n\nSUCCESS!" << endl;
	}

	whoCanBuy(7, db, zErrMsg);

	system("PAUSE");
	return 0;
}